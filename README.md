# android_prebuilt_apps

This repo contains prebuilt apk for the apps added in this fork of GrapheneOS.

This repo contains prebuilt apk for the following applications:
- DAVx5: https://github.com/bitfireAT/davx5-ose
- Etar: https://github.com/Etar-Group/Etar-Calendar
- F-Droid: https://gitlab.com/fdroid/fdroidclient
- F-DroidPrivilegedExtension: https://gitlab.com/fdroid/privileged-extension/
- GeometricWeather: https://github.com/WangDaYeeeeee/GeometricWeather
- Nextcloud: https://github.com/nextcloud/android
